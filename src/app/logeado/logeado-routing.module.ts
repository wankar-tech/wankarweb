import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Index2Component } from './index2/index2.component';
import { AuthGuard } from '../services/guards/auth.guard';
import { ProfileComponent } from './admin/profile/profile.component';
import { LayoutcursoComponent } from './layoutcurso/layoutcurso.component';
import { LeccionesComponent } from './lecciones/lecciones.component';
import { ClassTextComponent } from './clases/class-text.component';
import { SentenceCreateComponent } from './admin/createcore/sentence-create.component';
import { SentenceListComponent } from './admin/createcore/sentence-list.component';


const routes: Routes = [
  
  {    
    path: '',
    data: {
    title: 'Paginas Logeado'
  },

    children: [
      {
        path: 'index2',
        component: Index2Component,
        canActivate: [AuthGuard],
        data: {
          title: ''
        }
      },
      {
        path: 'profiles',
        component: ProfileComponent,
        //canActivate: [AuthGuard],
        data: {
          title: ''
        }
      },
       {
        path: 'clase-texto',
        component: ClassTextComponent,
        //canActivate: [AuthGuard],
        data: {
          title: ''
        }
      },
       {
        path: 'sentence-create',
        component: SentenceCreateComponent,
        //canActivate: [AuthGuard],
        data: {
          title: ''
        }
      },
      {
        path: 'sentence-list',
        component: SentenceListComponent,
        //canActivate: [AuthGuard],
        data: {
          title: ''
        }
      },
       {
        path: 'curso',
        component: LayoutcursoComponent,
        //canActivate: [AuthGuard],
        data: {
          title: 'Cursos'
        },
        children: [
          {
            path: '1',
            component: LeccionesComponent,
            data: {

            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogeadoRoutingModule {}
