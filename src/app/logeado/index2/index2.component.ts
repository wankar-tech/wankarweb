import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/auth/authentication.service';

@Component({
  templateUrl: 'index2.component.html'
})
export class Index2Component implements OnInit {

  mytoken: any = '';
  constructor(private _authenticationService: AuthenticationService){

  }
  ngOnInit(){
    let token  = localStorage.getItem('token');
    this.mytoken = token;
    console.log('logeado : ' + token);

  }

}
