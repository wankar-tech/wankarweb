import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

//interfaces
import { SentenceInterface } from '../../../interfaces/core/sentence.interface';
import { LanguageInterface } from '../../../interfaces/core/language.interface';
import { ClassInterface } from '../../../interfaces/core/class.interface';

import { SentenceService } from '../../../services/core/sentence.service';
import { LanguageService } from '../../../services/core/language.service';
import { ClassService } from '../../../services/core/class.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({

  templateUrl: './sentence-create.component.html'

})
export class SentenceCreateComponent implements OnInit {

  public idiomasarray: LanguageInterface[];
  public clasearray: ClassInterface[];
  public currentSelection: LanguageInterface;
  public currentSelectionC: ClassInterface;

    errorMessage: string;

	sentence: SentenceInterface = {
    idioma:'',
    clase:'',
    frase:''
  }
  constructor(private _sentenceService: SentenceService,
  						private _idiomaService: LanguageService,
              private _claseService: ClassService,
              private _route: ActivatedRoute,
              private _router: Router
  						) { }

  ngOnInit() {
    this.getClases();
    this.getIdiomas();
    
  }
	onGuardar(){
		console.log(this.sentence);
    this._sentenceService.newSentence( this.sentence)
    .subscribe( data=>{
      this._router.navigate(['/logeado/sentence-list']);
    }); 
  }
  //metodos para obtener datos para el combos
  getIdiomas() {
    this._idiomaService.getIdiomas()
                     .subscribe(
                       idioms => this.idiomasarray = idioms,
                       error =>  this.errorMessage = <any>error);
  }
  getClases() {
    this._claseService.getClases()
                     .subscribe(
                       clase => this.clasearray = clase,
                       error =>  this.errorMessage = <any>error);
  }
  onChange(value: LanguageInterface){
    //console.log('antes : ' + value.id);
    this.currentSelection = value;
    this.sentence.idioma = this.currentSelection.toString();
    console.log('despues : ' + this.currentSelection);
    console.log('idio : ' + this.sentence.idioma);
  }
   onChangeC(value: ClassInterface){
    //console.log('antes : ' + value.id);
    this.currentSelectionC = value;
    this.sentence.clase = this.currentSelectionC.toString();
    console.log('despues : ' + this.currentSelectionC);
    console.log('clase : ' + this.sentence.clase);
  }


}