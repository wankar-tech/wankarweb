import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
//interfaces
import { SentenceInterface } from '../../../interfaces/core/sentence.interface';
import { LanguageInterface } from '../../../interfaces/core/language.interface';
import { ClassInterface } from '../../../interfaces/core/class.interface';

import { SentenceService } from '../../../services/core/sentence.service';
import { LanguageService } from '../../../services/core/language.service';
import { ClassService } from '../../../services/core/class.service';

@Component({
  templateUrl: 'sentence-list.component.html',
  providers: [ SentenceService, LanguageService, ClassService]
})

export /**
 * SentenceListComponent
 */
class SentenceListComponent implements OnInit {
  errorMessage: string;
  sentences: SentenceInterface[];
  public title: string;
  public idiomasarray: LanguageInterface[];
  public clasearray: ClassInterface[];
  public currentSelection: LanguageInterface;
  public currentSelectionC: ClassInterface;
constructor(private _sentenceService: SentenceService,
              private _languageService: LanguageService,
              private _classService: ClassService,
              private route: ActivatedRoute,
              private router: Router
   ){
     this.title = 'Lista Frase';
     this.currentSelection = null;
  }
  ngOnInit(){
    this.getIdiomas();
    this.getClases();
  }
  onSubmit(){
    this.getFrases();

  }
  getFrases() {
    let idioma: any = this.currentSelection.toString();
    let clase: any = this.currentSelectionC.toString();
      this._sentenceService.getSentences(clase, idioma)
                       .subscribe(
                         frases => this.sentences = frases,
                         error =>  this.errorMessage = <any>error);
  }
  onDeleteConfirm(){
    console.log('Borrar Registro');
  }

  onChangeIdioma(value: LanguageInterface){
    this.currentSelection = value;
  }
  onChangeClase(value: ClassInterface){
    this.currentSelectionC = value;
  }

  getIdiomas() {
    this._languageService.getIdiomas()
                     .subscribe(
                       idioms => this.idiomasarray = idioms,
                       error =>  this.errorMessage = <any>error);
  }
  getClases() {
    this._classService.getClases()
                     .subscribe(
                       clase => this.clasearray = clase,
                       error =>  this.errorMessage = <any>error);
  }
}