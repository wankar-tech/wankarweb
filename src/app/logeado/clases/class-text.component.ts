import { Component, OnInit } from '@angular/core';
//servicios

import { ClassService } from '../../services/core/class.service';
import { Response } from '@angular/http';
import { WordtraslationClass, SentenceClass } from '../../clases/core/index';
import { WordClass } from '../../clases/core/word.class';



@Component({
  templateUrl: './class-text.component.html'
})
export class ClassTextComponent implements OnInit {
	/*Variables*/
  public incBar: number=0;
  
  public dataTexto:string = '';
  sentence: SentenceClass[];
  //public traduction: Array<WordClass> = [];
  public traduction: WordClass[]=[];
  public words: WordtraslationClass[]=[];
	public contador: number;
  public registroactual: string;
  public myfrase: string;
  constructor(public _serviceClass: ClassService) { 
    this.dataTexto = 'El dia esta hermoso';
    this.traduction=[];
    
  }
	
   //cargar metodo para test
	onLoadFrase(){
    this._serviceClass.loadFrase(1,1).subscribe(
			 response => {
         /*Crear objetos SentenceClass*/
        this.sentence = response;
					
          console.log(' ver data : ' + this.sentence[0].text_original);
         /* for(let i = 0; i < response.length; i++){
            console.log(response[i].text_original);
            
          }*/
         /* console.log('TEST Array SENTENCE : ' + this.sentence[0].orden + ' Palabra Ori : ' + 
          	this.sentence[0].text_original + ' Palabra Equivalente : '  );*/
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          console.log('wipayerror: ' + error);
        }
      }
    );
  }


	/*Crear un objeto Sentence-Frase */
  createSentence(){
		this.myfrase = this.sentence[0].text_original;
    //console.log(this.sentence);
  }

  //cargar metodo para test
	onLoadTraducction(frase: number, idioma: number){
    //this.traduction = null;
    this._serviceClass.loadTraduction(frase,idioma).subscribe(
			 response =>{
        console.log('llama a carga traducction : ' + response[0].name);
        let traducction: WordClass = new WordClass(response[0].orden, response[0].name);
        //this.traduction.push(traducction);
        this.traduction[0]= traducction;
         //console.log('VER Asigado traducction  : ' + this.traduction[0].name );
        //this.traduction = response;
        //return response;
        
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          console.log('wipayerror: ' + error);
        }
        //return null;
      }
    );
  }

 //cargar metodo para test
	onLoadPalabra(frase: number){
    let wordTraslation: WordtraslationClass[];
    this._serviceClass.loadPalabras(frase).subscribe(
			 response =>{
				wordTraslation = response;
        console.log(' componente - llama a carga Palabras para tooltip : ' + response[0].word + ' equivale : ' + response[0].traslation);
        for(let i = 0; i < wordTraslation.length; i++){
      		console.log(' odern : ' + wordTraslation[i].orden + ' ' + 
          ' palabra : ' + wordTraslation[i].word + ' ' +
          ' traducion : ' + wordTraslation[i].traslation);
          
    		}
      },
      error => {
        var errorMessage = <any>error;
        if(errorMessage != null){
          console.log('wipayerror: ' + error);
        }
      }
    );
  }

  onLoadTraslator(){
    this._serviceClass.loadTraslate(50,2).subscribe(
      data => {
        console.log('WSSSS: ' + data[0].traduccion);
      }
    );
  }

	/*Metodos y datos iniciales al cargar el componente*/
  ngOnInit() {
      //this._serviceClass.crearDatos();
      //this.onLoadTraducction();
      //this.onLoadPalabra();
      this.contador = 0;
      this.onLoadFrase();
      this.createSentence();
      
      //this.registroactual = this.sentence[0].text_original;

      //this.onLoadTraslator();
  }
  /*Metodos para evaluar clases*/
  onQualify(){

  }
  /*Metodo que permite continuar con la clase*/
  onContinue(){

  }
  /*Metodo para reproducir sonido*/
  onSoundPlay(){

  }

  /*Incrmento barra de progreso*/
	onIncrementProgress(){

  }

  /*limpiar elementos*/
  onClean(){
    
  }

  /*Eventos de botones calificar, continuar*/
  btnShowQualify(){

  }

	/**/
  btnHideQualifiy(){

  }



}