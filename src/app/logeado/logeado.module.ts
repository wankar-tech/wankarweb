import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { CommonModule } from '@angular/common';

import { Index2Component } from './index2/index2.component';
import { LogeadoRoutingModule } from './logeado-routing.module';
import { AuthenticationService } from '../services/auth/authentication.service';
import { AuthGuard } from '../services/guards/auth.guard';
import { ProfileComponent } from './admin/profile/profile.component';
import { LayoutcursoComponent } from './layoutcurso/layoutcurso.component';
import { LeccionesComponent } from './lecciones/lecciones.component';
import { ClassTextComponent } from './clases/class-text.component';
import { SentenceCreateComponent } from './admin/createcore/sentence-create.component';
import { SentenceListComponent } from './admin/createcore/sentence-list.component';

//Servicios
import { SentenceService } from '../services/core/sentence.service';
import { LanguageService } from '../services/core/language.service';
import { ClassService } from '../services/core/class.service';
@NgModule({
  imports: [ LogeadoRoutingModule, 
     					FormsModule,
    					HttpModule,
    					CommonModule
            ],
  declarations: [
    Index2Component,
    ProfileComponent,
    LayoutcursoComponent,
    LeccionesComponent,
    ClassTextComponent,
    SentenceCreateComponent,
    SentenceListComponent
],
  providers: [AuthenticationService, 
  						AuthGuard, 
              SentenceService,
              LanguageService,
              ClassService
              ]
})
export class LogeadoModule {


}
