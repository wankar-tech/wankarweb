export class WordClass {
     public orden: number;
	 public name: string;
    constructor(orden:number,
    			name: string){
		this.orden = orden;
        this.name = name;
    }
    public getOrden():number{
		return this.orden;
    }
    public getName():string{
		return this.name;
    }
    public setOrden(value: number){
        this.orden = value;

    }
     public setName(value: string){
        this.name = value;

    }
}