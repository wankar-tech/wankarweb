import { WordClass } from './word.class';
import { WordtraslationClass } from './wordtraslation.class';
export /**
 * Clase de Sentence Evaluator 
 */
class SentenceClass {
  orden: number;
  text_original: string;
  text_traslator: WordClass[];
  tooltip: WordtraslationClass[];
  sound?: string;
  image?: string;
  constructor(
      orden: number,
      text_original: string,
      text_traslator: Array <WordClass>,
      tooltip: WordtraslationClass[],
      sound?: string,
      image?: string
  ) { 
    this.orden = orden;
    this.text_original = text_original;
    this.text_traslator = text_traslator;
    this.tooltip = tooltip;
    this.sound = sound;
    this.image = image;
   }
   public getTextTraslator(){
     return this.text_traslator;
   }


}
