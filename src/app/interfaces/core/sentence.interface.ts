
export /**
 * Clase de Modelo de Fraces
 */
interface SentenceInterface {
     idioma: string;
     clase: string;
     frase: string;
}
