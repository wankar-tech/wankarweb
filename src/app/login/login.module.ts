import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';

// authguard
import { AuthGuard } from '../services/guards/auth.guard';
import { AuthenticationService } from '../services/auth/authentication.service';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [AuthGuard, AuthenticationService],
  declarations: [ LoginComponent ]
})
export class LoginModule { }
