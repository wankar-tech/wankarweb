import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../auth/authentication.service';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class AuthGuard implements CanActivate  {
    constructor(private _authService: AuthenticationService , 
    			private _router: Router){ }
    
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        if (localStorage.getItem('tasnet')){
			return true;
        }
        
        this._router.navigate(['/login']);
        return false;
    }
}