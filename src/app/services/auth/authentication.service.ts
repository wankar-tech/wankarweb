import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../global';

@Injectable()
export class AuthenticationService {
    public token: string;
  	public url: string;
    public isLoggedIn: boolean = false;
    constructor(private _http: Http) {
        // set token if saved in local storage
        this.url = GLOBAL.url;
        var currentUser = JSON.parse(localStorage.getItem('tasnet'));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string): Observable<boolean> {
        //va la url de conexion
		let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('username', username);
        urlSearchParams.append('password', password);
        let body = urlSearchParams.toString();
        console.log('DATOS ANTES DE LLAMAR HTTP: ' + body);
        //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
 		let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    	let options = new RequestOptions({ headers: headers });

        return this._http.post(this.url + '/auth/api/auth/', body, options )
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                //let token = response.json() && response.json().token;
                let _token = response.json();
                console.log('token antes comparacion : ' +  _token );
                if (_token) {
                    // set token property
                    this.token = _token;
					console.log('cuando token true : ' + _token);
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    //Guarda datos en el localStorage
                    localStorage.setItem('tasnet', JSON.stringify({ username: username, token: _token }));
                    localStorage.setItem('identity', JSON.stringify({username: username}));
                    localStorage.setItem('token', JSON.stringify(_token));
					console.log('LOCAL STORAGE : ' );
                    // return true to indicate successful login
                	this.isLoggedIn = true;
                    return true;
                } else {
                    // return false to indicate failed login
                    console.log('cuando token false: ');
                    this.isLoggedIn = false;
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.isLoggedIn = false;
        this.token = null;
        localStorage.removeItem('tasnet');
    }
}