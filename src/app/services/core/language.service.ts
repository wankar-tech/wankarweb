import { Injectable } from '@angular/core';
import {Http, Response, Headers,
        RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import { GLOBAL } from '../global';
import { Params } from '@angular/router';
// Interfaces

import { LanguageInterface } from '../../interfaces/core/language.interface';


@Injectable()
export class LanguageService {
    public url: string;


	constructor(private _http: Http) { 
        this.url = GLOBAL.url;
    }

   getIdiomas(): Observable<LanguageInterface[]>{
    return this._http.get(this.url + '/frase/api/idiomas/')
                .map(response => response.json())
                .catch(this.handleError);
   }
	
    private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      //alert('DENTRO CATCH..'+errMsg);
      //console.error('DENTRO CATCH : '+errMsg);
      return Observable.throw(errMsg);
    }
}