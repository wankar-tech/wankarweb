import { Injectable } from '@angular/core';
import {Http, Response, Headers,
        RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import { GLOBAL } from '../global';
import { Params } from '@angular/router';

import { ClassInterface } from '../../interfaces/core/class.interface';
//clases
import { WordtraslationClass, WordClass, SentenceClass } from '../../clases/core/index';
import { IdiomaModule } from '../../registro/idioma/idioma.module';


@Injectable()
export class ClassService {
	/*Variables*/


  public url: string;
  private text_traslation: WordClass[]=[];
  private text_palabratootip: WordtraslationClass[]=[];

  private  palabrasG: WordClass[]=[];
  
  constructor(private _http: Http) {
    this.url = GLOBAL.url;
  }
  getClases(): Observable<ClassInterface[]>{
    return this._http.get(this.url + '/frase/api/clases/curso/1/')
                .map(response => response.json())
                .catch(this.handleError);
  }


    private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return Observable.throw(errMsg);
  }
  /**
   * Crear Objeto Sentence
   */
  createSentence(){

  }

  /**
   * Cargar Frase
   */
   loadFrase(clase:number, idioma: number): Observable<SentenceClass[]>{
    //let traduction = new WordtraslationClass(1,'');
    let traductionArray: SentenceClass[]=[];
    return this._http.get(this.url + '/frase/api/frases/' + clase + '/' + idioma )
                .map(response => this.objJsonToObjDataFrase(response));
  }

	/**
   * Cargar Frases
   */
   private objJsonToObjDataFrase(dataRes: Response){
    let data = dataRes.json(); 
    let objWord: SentenceClass[]=[];
    /*console.log('nro registros: ' + data.length + ' ID : ' + data[0].id + 
    ' FRASE : ' + data[0].frase + ' IDIOMA : ' + data[0].idioma.nombre);*/
    
     //this.loadTraslate(48,1);

     //console.log('carga de traduccion 48 : ' );
    /*carga json en un objeto en un array*/
    for(let i = 0; i < data.length; i++){
      //crear word.class
			//let wordClass: WordClass[]=[];
			//console.log('DENTRO FOR : ' + i);
      //this.loadTraduction(data[i].id, data[i].idioma.id);
      //console.log('DENTRO FOR ID : ' + data[i].frase);
      //this.loadPalabras(data[i].id);
      //crear wordtraslation.clas
			//console.error('traductiones : ' + this.text_traslation[0].orden);
      objWord[i] = new SentenceClass(data[i].id,
                                  data[i].frase, 
                                  this.text_traslation, 
                                  this.text_palabratootip,
                                  'http://mymusica.mp3',
                                  'http://myimagen.jpe');
    }
    //console.log('converson : ' + objWord[0].orden + ' Frase :' + objWord[0].text_original);
    //asigna objeto a una variable
     //this.text_traducction = objWord;
    //this.loadTraslate(50,2);
    //console.log(objWord.toString());
    //console.log('FRASE : Despues de Asignar a Variable local Objeto word.class: ' + 
    //objWord[0].sound + ' traduccion : ' );
    return objWord;
	}


	private extractData(res: Response){
    let body = res.json();
    console.log('datos en extracted : ' + body)
    return body.data || {};
  }


  loadTraslate(frase:number, idioma:number){
    return this._http.get(this.url + '/frase/api/traducciones/' + frase + '/' + idioma)
                .map(response =>   
                  	response.json()
                 );
  }
	
  public objData(dataRes: Response){
    let data = dataRes.json(); 
    let palabras: WordClass[]=[];
    /*console.log('nro registros: ' + data.length + ' ID : ' + data[0].id + 
    ' TRADUCCION : ' + data[0].traduccion + ' FRASE : ' + data[0].frase.frase);*/
    /*carga json en un objeto en un array*/
    for(let i = 0; i < data.length; i++){
      palabras[i] = new WordClass(data[i].id,
                                  data[i].traduccion);
    }
    //console.log('Asigno NEW  : ' + palabras[0].name);
    //asigna objeto a una variable
     this.palabrasG = palabras;
    //console.log('Asignar Data - palabras : ' + this.palabrasG[0].name);
    
	}

  /**
   * Carga Word.class
   * params frase, idioma
   */
  loadTraduction(frase:number, idioma:number): Observable<WordClass[]>{
    //let traduction = new WordClass(1,'');
   // let traductionArray: WordClass[]=[];

    return this._http.get(this.url + '/frase/api/traducciones/' + frase + '/' + idioma)
                .map(response => this.objJsonToObjDataTraducction(response)
                );


  }
 /**
 * Metodo que recibe un json(traduccion) y convierte en Array Objeto Traduccion
 * retorna objeto tipo traduccion(traduccion idioma uno a idioma dos)
 */
  private objJsonToObjDataTraducction(dataRes: Response){
    let data = dataRes.json(); 
    let objWord: WordClass[]=[];
    /*console.log('nro registros: ' + data.length + ' ID : ' + data[0].id + 
    ' TRADUCCION : ' + data[0].traduccion + ' FRASE : ' + data[0].frase.frase);*/
    /*carga json en un objeto en un array*/
    for(let i = 0; i < data.length; i++){
      objWord[i] = new WordClass(data[i].id,
                                  data[i].traduccion);
    }
    //console.log('Asigno NEW  : ' + objWord[0].name);
    //asigna objeto a una variable
     this.text_traslation = objWord;
    //console.log('Despues de Asignar a Variable local : ' + this.text_traslation[0].name);
    return objWord;
	}

  loadPalabras(frase:number): Observable<WordtraslationClass[]>{
    //let traduction = new WordtraslationClass(1,'');
    let traductionArray: WordtraslationClass[]=[];
    return this._http.get(this.url + '/frase/api/palabras/frase/' + frase + '/' )
                .map(response => this.objJsonToObjDataPalabras(response));
  }



 /**
 * Metodo que recibe un json(traduccion) y convierte en Array Objeto Traduccion
 * retorna objeto tipo traduccion(traduccion idioma uno a idioma dos)
 */
  private objJsonToObjDataPalabras(dataRes: Response){
    let data = dataRes.json();
    let objWord: WordtraslationClass[]=[];
    //console.log('nro registros: ' + data.length + ' ID : ' + data[0].palabra + 
    //' TRADUCCION : ' + data[0].traduccion + ' FRASE : ' + data[0].frase.id);
    /*carga json en un objeto en un array*/
    for(let i = 0; i < data.length; i++){
      objWord[i] = new WordtraslationClass(data[i].id,
                                  data[i].palabra, data[i].traduccion);
    }
    //console.log('converson : ' + objWord[0].word + 'traduccion : ' + objWord[0].traslation);
    //asigna objeto a una variable tooltip
    this.text_palabratootip = objWord;
    //console.log('Despues de Asignar a Variable local : ' + this.text_palabratootip[0].word);
    return objWord;
  }



}