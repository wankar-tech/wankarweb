import { Injectable } from '@angular/core';
import {Http, Response, Headers,
        RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import { GLOBAL } from '../global';
import { UrlList } from '../url-list';
import { Params, Router } from '@angular/router';

//modelos
import { LanguageInterface } from '../../interfaces/core/language.interface';
import { ClassInterface } from '../../interfaces/core/class.interface';
import { SentenceInterface } from '../../interfaces/core/sentence.interface';

@Injectable()

export /**
 * SentenceService
 */
class SentenceService {

    /*Variables*/
  public url: string;
  public urlFraseGetAll: string;
  public urlFrasePostAdd: string;
  public urlFraseGetById: string;
  public clase: ClassInterface ;
  public idioma: LanguageInterface;
  constructor (private _http: Http) {
    this.url = GLOBAL.url;
    this.urlFraseGetAll = UrlList.fraseGetFraseAll;
    this.urlFrasePostAdd = UrlList.frasePostAdd;

  }

/**Obtener datos formateados */
	

    /**
   * Devuelve todas las frases
   * recibe como pararametro CLASE e IDIOMA
   */
  getSentences (clase: any, idioma: any): Observable<SentenceInterface[]> {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers });
    return this._http.get(this.url + this.urlFraseGetAll + clase + '/' + idioma + '/', options)
                    .map(response => response.json())
                    .catch(this.handleError);
  }
  /**Busca una frase
   * params ID Frase
  */
  getSentenceById(fraseId: any){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({headers: headers});
    return this._http.get(this.url + this.urlFraseGetAll + '/' + fraseId, options)
                      .map(response => response.json())
                      .catch(this.handleError);
  }

  /**
   * Agrega una Frase
   */
  addSentence(fraseadd_obj): Observable<SentenceInterface>{
      let urlSearchParams = new URLSearchParams();
      urlSearchParams.append('frase', fraseadd_obj.frase);
      urlSearchParams.append('clase', fraseadd_obj.clase);
      urlSearchParams.append('idioma', fraseadd_obj.idioma);
      let bodys = urlSearchParams.toString();
      console.log('datos recibido: ' + bodys);
      let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      return this._http.post(this.url + this.urlFrasePostAdd , bodys, options)
          .map(this.extractData).catch(this.handleError);
  }
  /*Agregar un Usuario - Interfaces*/
  newSentence(_sentence: SentenceInterface){
    let urlSearchParams = new URLSearchParams();
      urlSearchParams.append('idioma', _sentence.idioma);
      urlSearchParams.append('clase', _sentence.clase);
      urlSearchParams.append('frase', _sentence.frase);
		let body = urlSearchParams.toString()
    console.log('antes de stringify : ' + _sentence);
    //let bodys = JSON.stringify(_sentence);
    //console.log('antes de headers' + bodys);
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
		let options = new RequestOptions({ headers: headers });
    return this._http.post( this.url + this.urlFrasePostAdd, body, { headers: headers })
    .map( res => {
      console.log('Resultado : ' + res.json());
      return res.json();
    })
    
  }
/**
   * Agrega una Frase
   */
  editSentence(fraseadd_obj,fraseId): Observable<SentenceInterface>{
      let urlSearchParams = new URLSearchParams();
      urlSearchParams.append('frase', fraseadd_obj.frase);
      urlSearchParams.append('clase', fraseadd_obj.clase);
      urlSearchParams.append('idioma', fraseadd_obj.idioma);
      let bodys = urlSearchParams.toString();
      console.log('datos recibido: ' + bodys);
      let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
      let options = new RequestOptions({ headers: headers });
      return this._http.put(this.url + this.urlFrasePostAdd + fraseId , bodys, options)
          .map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response){
    let body = res.json();
    console.log('datos en extracted : ' + body)
    return body.data || {};
  }
  private handleError (error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.log('error dentro : ' + errMsg);
      return Observable.throw(errMsg);
  }
 

}