export /**
 * Clase de Modelo de Fraces
 */
class SentenceModel {
  constructor(
    public id: string,
    public clase: number,
    public idioma: string,
    public frase: string
  ) { }

}
