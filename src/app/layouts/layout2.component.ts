import { Component, OnInit } from '@angular/core';

//servicio
import { AuthenticationService } from '../services/auth/authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-layout2',
  templateUrl: './layout2.component.html',
})
export class Layout2Component implements OnInit {
	identity: any = '';
  constructor(private _authenticationService: AuthenticationService, private _router: Router) { }

  ngOnInit(): void { 
    this.identity = JSON.parse( localStorage.getItem('identity'));
    //console.log('nombre usuario en Layout2Component : ' + this.username.username);
  }
	
  onSalir(){
		this._authenticationService.logout();
		this._router.navigate(['/']);
  }
}
